#MAINTAINER Bayu Aji <surat@bayuaji.id>

# -----Base Node-----
FROM node:alpine3.14 AS base
# install node
# Check https://github.com/nodejs/docker-node/tree/b4117f9333da4138b03a546ec926ef50a31506c3#nodealpine to understand why libc6-compat might be needed.
RUN apk add --no-cache nodejs-current tini libc6-compat
# Set woring directory
WORKDIR /app
# Set tini as entrypoint
ENTRYPOINT ["/sbin/tini", "--"]
# Copy project file
COPY package.json .

# -----Dependencies-----
FROM base AS dependencies
# Install node packages
RUN npm set progress=false && npm config set depth 0
RUN npm install --only=production
# Copy production node_modules aside
RUN cp -R node_modules prod_node_modules
# Install all node_modules, including 'devDependencies'
RUN npm install

## -----Test------
#FROM dependencies AS test
#COPY . .
## Run linters, setup and tests
#RUN  npm run lint && npm run setup && npm run test

#
# ---- Release ----
FROM base AS release
# Copy production node_modules
COPY --from=dependencies /app/prod_node_modules ./node_modules
# Copy app sources
COPY . .
# Expose port and define CMD
EXPOSE 3000
CMD npm run start

